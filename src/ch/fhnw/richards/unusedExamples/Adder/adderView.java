package ch.fhnw.richards.unusedExamples.Adder;

import javafx.animation.ScaleTransition;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class adderView {
	private adderModel model;
	private Stage stage;
	// Set up controls
	protected Label lblTotal = new Label();
	protected Button btnAdd = new Button("Add amount to total");
	protected TextField txtAmount = new TextField("0");
	private ScaleTransition pulse;

	protected adderView(Stage stage, adderModel model) {
		this.stage = stage;
		this.model = model;
		// Bind lblTotal, so that it automatically updates from the model
		lblTotal.textProperty().bind(model.getValueProperty().asString());
		// Create layout
		Pane root = createLayout();
		// Prepare animation for use
		pulse = createAnimation();
		// Set up stage and scene
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Adder");
	}

	/**
	 * Method to set up the layout
	 */
	private Pane createLayout() {
		VBox vBox = new VBox();
		Region spacer = new Region();
		btnAdd.setMaxWidth(Double.MAX_VALUE);
		VBox.setVgrow(spacer, Priority.ALWAYS);
		vBox.getChildren().addAll(txtAmount, btnAdd, spacer, lblTotal);
		vBox.setPadding(new Insets(10));
		return vBox;
	}

	/**
	 * Method to set up the animation: The label should increase in size by 50%, and
	 * then decrease back to normal.
	 */
	private ScaleTransition createAnimation() {
		ScaleTransition grow = new ScaleTransition(Duration.millis(500), lblTotal);
		grow.setToX(1.5);
		grow.setToY(1.5);
		grow.setCycleCount(2);
		grow.setAutoReverse(true);
		return grow;
	}

	/**
	 * Method to display the GUI, after all MVC initialization is complete
	 */
	public void start() {
		stage.show();
	}

	/**
	 * Getter for the stage, so that the controller can access window events
	 */
	protected Stage getStage() {
		return stage;
	}

	/**
	 * Method to allow the controller to trigger an animation
	 */
	protected void doAnimate() {
		pulse.play();
	}
}
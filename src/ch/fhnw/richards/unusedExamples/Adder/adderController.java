package ch.fhnw.richards.unusedExamples.Adder;

public class adderController {
	final private adderModel model;
	final private adderView view;

	protected adderController(adderModel model, adderView view) {
		this.model = model;
		this.view = view;
		// Register ourselves to listen for property changes in the model.
		// If the new value is below zero, we call view.doAnimate();
		model.getValueProperty().addListener((obs, oldValue, newValue) -> {
			if (((Integer) newValue) < 0) view.doAnimate();
		});
		

		// register ourselves to listen for button clicks. When the button
		// is clicked, what whatever is needed for the application to work
		view.btnAdd.setOnAction((event) -> {
			model.setValue(model.getValue() + Integer.parseInt(view.txtAmount.getText()));
		});
	}
}
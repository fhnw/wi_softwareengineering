package ch.fhnw.richards.topic01_JavaFX_MVC.tutorial.v2;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class Mines_Controller {
	private Mines_Model model;
	private Mines_View view;
	
	// The controller is responsible for width and height of the game grid.
	// Initially, these will be constants. Later, they may be taken from the
	// View as game options chosen by the player.
	private int width = 10;
	private int height = 8;

	public Mines_Controller(Mines_Model model, Mines_View view) {
		this.model = model;
		this.view = view;
		
		this.newGame();
	}

	private void newGame() {
		model.newGame(width, height);
		view.newGame(width, height);
		
		// Add Button-Click listeners
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				view.buttons[row][col].setOnMouseClicked(this::mouseClick);
			}
		}
	}
	
	private void mouseClick(MouseEvent e) {
		// Which cell was clicked (row, col)
		int row = -1;
		int col = -1;
		for (int r = 0; row == -1 && r < height; r++) {
			for (int c = 0; col == -1 && c < width; c++) {
				if (e.getSource() == view.buttons[r][c]) {
					row = r; col = c;
				}
			}
		}
		
		// Left-click or right-click?
		if (e.getButton() == MouseButton.PRIMARY) {
			// Reveal this field in the model, and update the UI
			// Note: throws an exception if this is a bomb!
			try {
				int numBombs = model.revealCell(row, col);
				view.buttons[row][col].setText(Integer.toString(numBombs));
			} catch (Mines_Model.Boom b) {
				view.showBoom();
			}
			
		} else if (e.getButton() == MouseButton.SECONDARY) {
			// Mark this button in the UI
			String oldText = view.buttons[row][col].getText();
			if (oldText.isEmpty())
				view.buttons[row][col].setText(Character.toString((char) 0x2691));
			else
				view.buttons[row][col].setText("");
		} else {
			// Some other button, so ignore it
		}
		
		if (model.isGameOver()) view.showGameOver();
	}
}

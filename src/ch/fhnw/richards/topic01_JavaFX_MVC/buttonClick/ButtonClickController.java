package ch.fhnw.richards.topic01_JavaFX_MVC.buttonClick;

public class ButtonClickController {

	private final ButtonClickModel model;
	private final ButtonClickView view;

	protected ButtonClickController(ButtonClickModel model, ButtonClickView view) {
		this.model = model;
		this.view = view;

		// register ourselves to listen for button clicks
		view.btnClick.setOnAction((event) -> {
			model.incrementValue();
			String newText = Integer.toString(model.getValue());
			view.lblNumber.setText(newText);
		});
	}
}

This is a console-based network client for testing text-based services.

Example: in Fall 2021 there was a project to implement a Todo-service.
This client could connect to the server at javaproject.ch:50002.

You could then enter commands, as specified in the project documentation.
The program displays the commands sent and the answers sent by the server.

package ch.fhnw.richards.topic03_JavaFX_PropertiesBindings.solutions.petExercise.v3;

import ch.fhnw.richards.topic03_JavaFX_PropertiesBindings.solutions.petExercise.Pet;
import javafx.beans.property.SimpleObjectProperty;

public class PetModel {
	private final SimpleObjectProperty<Pet> petProperty = new SimpleObjectProperty<>();
	
	public void savePet(Pet.Species species, Pet.Gender gender, String name) {
		petProperty.set(new Pet(species, gender, name));
	}
	
	public void deletePet() {
		petProperty.set(null);
	}
	
	public Pet getPet() {
		return petProperty.get();
	}
	
	public SimpleObjectProperty<Pet> petProperty() {
		return petProperty;
	}
}

package ch.fhnw.richards.topic08_DataStructures.solutions.DataStructureTesting;

import javafx.application.Application;
import javafx.stage.Stage;

public class DataStructureTesting extends Application {
    private DataStructureTestingView view;
    private DataStructureTestingController controller;
    private DataStructureTestingModel model;

    @Override
    public void start(Stage stage) throws Exception {
        model = new DataStructureTestingModel();
        view = new DataStructureTestingView(stage, model);
        controller = new DataStructureTestingController(model, view);

        // Display the GUI after all initialization is complete
        view.start();
    }

    public static void main(String[] args) {
        launch(args);
    }

}

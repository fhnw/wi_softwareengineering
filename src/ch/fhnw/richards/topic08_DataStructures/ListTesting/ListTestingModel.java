package ch.fhnw.richards.topic08_DataStructures.ListTesting;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListTestingModel {
	protected enum LIST_TYPES {ArrayList, LinkedList};
	protected enum WHERE_CHOICES {Start, Middle, End};
	
	/**
	 * Run the defined test, returning the total elapsed time in seconds
	 * @param amountOfData The amount of elements to add to the list
	 * @param listType The type of list to use
	 * @param whereChoice Where to insert the elements in the list
	 * @return the elapsed time in seconds, as a float
	 */
	public float runTest(Integer amountOfData, LIST_TYPES listType, WHERE_CHOICES whereChoice) {
		// Create the data objects in an array
		SampleData[] data = createData(amountOfData);		
		List<SampleData> list = createList(amountOfData, listType);
		
		// Perform the test
		long startTime = System.currentTimeMillis();
		addDataToList(data, list, whereChoice);
		long endTime = System.currentTimeMillis();
		return (endTime - startTime) / 1000.0f;
	}

	private SampleData[] createData(Integer amountOfData) {
		SampleData[] data = new SampleData[amountOfData];
		for (int i = 0; i < amountOfData; i++) data[i] = new SampleData();
		return data;
	}
	
	private List<SampleData> createList(Integer amountOfData, LIST_TYPES listType) {
		// Create an empty list of the desired type
		List<SampleData> list = null;
		if (listType == LIST_TYPES.ArrayList)
			list = new ArrayList<>();
		else if (listType == LIST_TYPES.LinkedList)
			list = new LinkedList<>();	
		return list;
	}
	
	private void addDataToList(SampleData[] data, List<SampleData> list, WHERE_CHOICES whereChoice) {
		for (SampleData element : data) {
			int index = 0; // Assume WHERE_CHOICES.Start
			if (whereChoice == WHERE_CHOICES.Middle)
				index = list.size() / 2;
			else if (whereChoice == WHERE_CHOICES.End)
				index = list.size();
			list.add(index, element);
		}
	}
}

package ch.fhnw.richards.topic21_JavaFX_Dialogs;

public class DataClass {
	private String pet;
	private String owner;

	public DataClass(String pet, String owner) {
		this.pet = pet;
		this.owner = owner;
	}

	@Override
	public String toString() {
		return pet + "/" + owner;
	}

	public String getPet() {
		return pet;
	}

	public String getOwner() {
		return owner;
	}
}

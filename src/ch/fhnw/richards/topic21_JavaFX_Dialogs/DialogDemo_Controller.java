package ch.fhnw.richards.topic21_JavaFX_Dialogs;

import java.util.ArrayList;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextInputDialog;
import javafx.util.Callback;

public class DialogDemo_Controller {
	private DialogDemo_View view;
	private DialogDemo_Model model;
	
	public DialogDemo_Controller(DialogDemo_View view, DialogDemo_Model model) {
		this.view = view;
		this.model = model;
		
	    view.btnAlertInfo.setOnAction(this::doAlertInfo);
	    view.btnAlertConfirmation.setOnAction(this::doAlertConfirmation);
	    view.btnAlertError.setOnAction(this::doAlertError);
	    view.btnAlertWarning.setOnAction(this::doAlertWarning);
	    view.btnAlertNone.setOnAction(this::doAlertNone);
	    view.btnChoiceDialog.setOnAction(this::doChoiceDialog);
	    view.btnTextInput.setOnAction(this::doTextInput);
	    view.btnCustomDialog.setOnAction(this::doCustomDialog);		
	}

	private void doAlertInfo(ActionEvent e) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert info title");
		alert.setHeaderText("Information Alert");
		alert.setContentText("This is an information alert.");
		Optional<ButtonType> result = alert.showAndWait(); // show() allows the program to continue immediately
        displayResult(result);
	}
	
	private void doAlertConfirmation(ActionEvent e) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Alert confirm title");
		alert.setHeaderText("Confirmation Alert");
		alert.setContentText("This is a confirmation alert.");
		Optional<ButtonType> result = alert.showAndWait(); // show() allows the program to continue immediately
        displayResult(result);
	}

	private void doAlertError(ActionEvent e) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Alert error title");
        alert.setHeaderText("Error Alert");
        alert.setContentText("This is an error alert.");
        Optional<ButtonType> result = alert.showAndWait(); // show() allows the program to continue immediately
        displayResult(result);
	}

	private void doAlertWarning(ActionEvent e) {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Alert warning title");
        alert.setHeaderText("Warning Alert");
        alert.setContentText("This is a warning alert.");
        Optional<ButtonType> result = alert.showAndWait(); // show() allows the program to continue immediately
        displayResult(result);
	}
	
	private void doAlertNone(ActionEvent e) {
        Alert alert = new Alert(AlertType.NONE); // DIY alert - add the buttons we want
        alert.setTitle("Alert none title");
        alert.setHeaderText("None Alert");
        alert.setContentText("This is a custom alert with one button.");
        alert.getDialogPane().getButtonTypes().add(ButtonType.OK); // We want an OK button
        Optional<ButtonType> result = alert.showAndWait(); // show() allows the program to continue immediately
        displayResult(result);
	}
	
	private void doChoiceDialog(ActionEvent e) {
        ArrayList<String> choices = new ArrayList<>();
        choices.add("First choice"); choices.add("Second choice"); choices.add("Third choice");  
        ChoiceDialog<String> choiceDialog = new ChoiceDialog<>("Second choice", choices);
        choiceDialog.setTitle("Choice dialog");
        choiceDialog.setHeaderText("Instructions");
        Optional<String> result = choiceDialog.showAndWait(); // show() allows the program to continue immediately
        displayResult(result);
	}
	
	private void doTextInput(ActionEvent e) {
        TextInputDialog textInputDialog = new TextInputDialog("Default value");
        textInputDialog.setTitle("Text input dialog");
        textInputDialog.setHeaderText("Instructions");
        Optional<String> result = textInputDialog.showAndWait(); // show() allows the program to continue immediately
        displayResult(result);
	}
	
	private void doCustomDialog(ActionEvent e) {
        Dialog<DataClass> customDialog = new Dialog<>();
        customDialog.setTitle("Custom dialog");
        customDialog.setHeaderText("Instructions");
        customDialog.setResizable(true);
        
        // Set up dialog content
        CustomDialogView dialogView = new CustomDialogView();
        customDialog.getDialogPane().setContent(dialogView);
        
        // Set up dialog buttons
        ButtonType btnCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
        ButtonType btnOK = new ButtonType("OK", ButtonData.OK_DONE);
        customDialog.getDialogPane().getButtonTypes().add(btnCancel);
        customDialog.getDialogPane().getButtonTypes().add(btnOK);
        
        // ResultConverter: Create the results to be returned, when OK is clicked
        customDialog.setResultConverter(new Callback<ButtonType, DataClass>() {
            @Override
            public DataClass call(ButtonType btn) {
                DataClass result = null;
                if (btn == btnOK) {
                    result = new DataClass(dialogView.txtPet.getText(), dialogView.txtOwner.getText());
                }
                return result;
            }
        });
        
        Optional<DataClass> result = customDialog.showAndWait(); // show() allows the program to continue immediately
        displayResult(result);
	}


	private void displayResult(Optional<?> result) {
        String resultText = "'null' result";
        if (result != null) {
            if (!result.isPresent()) {
                resultText = "Optional contains 'null' result";
            } else {
                resultText = result.get().toString();
            }
        }
        view.lblResult.setText(resultText);
	}
}

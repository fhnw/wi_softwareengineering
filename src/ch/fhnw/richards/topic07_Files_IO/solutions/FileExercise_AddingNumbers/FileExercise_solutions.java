package ch.fhnw.richards.topic07_Files_IO.solutions.FileExercise_AddingNumbers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This solution file only contains the single method "readFile". Place this method
 * in the model, to complete the solution.
 * 
 * Note: This file contains two different solutions.
 */
public class FileExercise_solutions {
	private static String NUMBERS_FILE = "numbers.txt";

	private ArrayList<Integer> numbers = null;
	
	/**
	 * This is the solution using Reader classes, as discussed in the lecture
	 */
	public void readFile_v1() {
		InputStream inStream = this.getClass().getResourceAsStream(NUMBERS_FILE);
		try (BufferedReader fileIn = new BufferedReader(new InputStreamReader(inStream))) {
			numbers = new ArrayList<>();
			String line = fileIn.readLine();
			while (line != null) {
				int number = Integer.parseInt(line);
				numbers.add(number);
				line = fileIn.readLine();
			}
		} catch (IOException e) {
			numbers = null;
		}
	}

	/**
	 * For text-based input, you can also use a Scanner
	 */
	public void readFile_v2() {
		InputStream inStream = this.getClass().getResourceAsStream(NUMBERS_FILE);
		try (Scanner in = new Scanner(inStream)) {
			numbers = new ArrayList<>();
			while (in.hasNext()) {
				int number = in.nextInt();
				numbers.add(number);
			}
		} catch (Exception e) {
			numbers = null;
		}
	}

}

package ch.fhnw.richards.topic07_Files_IO.FileWithPets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Our model only contains a bunch of strings
 */
public class PetExample_Model {
	private static String PETS_FILE = "pets.txt";
	private static String SEPARATOR = ";"; // Separator for "split"

	private ArrayList<Pet> pets = null;

	public ArrayList<Pet> getPets() {
		return pets;
	}

	public void addPet(Pet pet) {
		pets.add(pet);
	}

	/**
	 * When loading pets, we overwrite any existing array.
	 */
	public void loadPets() {
		File petsFile = new File(PETS_FILE);
		try (Reader inReader = new FileReader(petsFile)) {
			BufferedReader in = new BufferedReader(inReader);
			pets = new ArrayList<Pet>();

			String line = in.readLine();
			while (line != null) {
				Pet pet = readPet(line);
				pets.add(pet);
				line = in.readLine();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Convert a String into a Pet
	 * To keep this example simple: no error handling
	 */
	private Pet readPet(String line) {
		String[] attributes = line.split(SEPARATOR);
		int ID = Integer.parseInt(attributes[0]);
		String name = attributes[1];
		Pet pet = new Pet(ID);
		pet.setName(name);
		return pet;
	}

	/**
	 * When saving pets, we overwrite any existing file
	 */
	public void savePets() {
		File petsFile = new File(PETS_FILE);
		try (Writer out = new FileWriter(petsFile)) {
			for (Pet pet : pets) {
				String line = writePet(pet);
				out.write(line);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * With this simple class, this is a simple method. More complex classes might
	 * require more complex processing
	 */
	private String writePet(Pet pet) {
		String line = pet.getID() + SEPARATOR + pet.getName() + "\n";
		return line;
	}
}

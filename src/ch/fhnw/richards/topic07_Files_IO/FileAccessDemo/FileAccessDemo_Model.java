package ch.fhnw.richards.topic07_Files_IO.FileAccessDemo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Our model only contains a bunch of strings
 */
public class FileAccessDemo_Model {
	private static String SAVE_FILE = "FileAccess_SaveFile.sav";
	private static String PACKAGE_FILE = "FileAccess_ThisPackage.txt";
	private static String OTHER_PACKAGE = "sampleFiles";
	private static String OTHER_PACKAGE_FILE = "FileAccess_OtherPackage.txt";
	
	private String saveFileData;
	private String packageFileData;
	private String otherPackageData;
	
	public FileAccessDemo_Model() {
		readPackageFile();
		readOtherPackageFile();
	}
	
	public void readPackageFile() {
        InputStream inStream = this.getClass().getResourceAsStream(PACKAGE_FILE);
		String data = "";
		try(BufferedReader fileIn = new BufferedReader(new InputStreamReader(inStream))) {
			data = fileIn.readLine();
		} catch (IOException e) {
			data = e.getClass().toString();
		}
		this.packageFileData = data;
	}
	
	public void readOtherPackageFile() {
        InputStream inStream = this.getClass().getResourceAsStream("/" + OTHER_PACKAGE + "/" + OTHER_PACKAGE_FILE);
		String data = "";
		try(BufferedReader fileIn = new BufferedReader(new InputStreamReader(inStream))) {
			data = fileIn.readLine();
		} catch (IOException e) {
			data = e.getClass().toString();
		}
		this.otherPackageData = data;
	}
	
	public void readSaveFile() {
		File file = new File(SAVE_FILE);
		String data = "";
		try(BufferedReader fileIn = new BufferedReader(new FileReader(file))) {
			data = fileIn.readLine();
		} catch (FileNotFoundException e) {
			data = "Save file does not exist";
		} catch (IOException e) {
			data = e.getClass().toString();
		}
		this.saveFileData = data;
	}
	
	public void writeSaveFile() {
		File file = new File(SAVE_FILE);
		try(FileWriter fileOut = new FileWriter(file)) {
			fileOut.write(saveFileData + "\n");
			fileOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setSaveFile(String in) {
		saveFileData = "Save file contents: " + in;
	}
	
	public String getSaveFile() {
		return saveFileData;
	}
	public String getPackageFile() {
		return packageFileData;
	}
	public String getOtherPackageFile() {
		return otherPackageData;
	}
}

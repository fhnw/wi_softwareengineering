package ch.fhnw.richards.topic11_Threads.solutions.CountUpDown_Tasks;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class CountUpDown_View {
    private CountUpDown_Model model;
    private Stage stage;

	protected Label lblNumber;
	protected Button btnUp;
	protected Button btnDown;

	protected CountUpDown_View(Stage stage, CountUpDown_Model model) {
		this.stage = stage;
		this.model = model;
		
		stage.setTitle("FX Threads Example");
		
		GridPane root = new GridPane();
		lblNumber = new Label();
		lblNumber.setText(Integer.toString(model.getValue().get()));
		root.add(lblNumber, 0, 0);
		
		btnUp = new Button("Count up!");
		root.add(btnUp, 0, 1);

		btnDown = new Button("Count down!");
		root.add(btnDown, 0, 2);

		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("CountUpDown.css").toExternalForm());
		stage.setScene(scene);;
	}
	
	public void start() {
		stage.show();
	}
	
	/**
	 * Stopping the view - just make it invisible
	 */
	public void stop() {
		stage.hide();
	}
	
	/**
	 * Getter for the stage, so that the controller can access window events
	 */
	public Stage getStage() {
		return stage;
	}
}
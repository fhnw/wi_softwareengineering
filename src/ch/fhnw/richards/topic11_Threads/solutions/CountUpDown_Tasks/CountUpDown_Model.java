package ch.fhnw.richards.topic11_Threads.solutions.CountUpDown_Tasks;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.concurrent.Task;

public class CountUpDown_Model {
    private SimpleIntegerProperty value;
    
    protected CountUpDown_Model() {
        value = new SimpleIntegerProperty(0);
    }

    public SimpleIntegerProperty getValue() {
        return value;
    }
    
    public void countUp() {
        Task<Void> task = new CountUpTask();
        new Thread(task, "Counting up").start();
    }

    public void countDown() {
        Task<Void> task = new CountDownTask();
        new Thread(task, "Counting down").start();
    }

    private class CountUpTask extends Task<Void> {
        @Override
        protected Void call() throws Exception {
        	for (int i = 0; i < 100; i++) {
                value.set(value.get() + 1);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
            return null;
        }
    }

    private class CountDownTask extends Task<Void> {
        @Override
        protected Void call() throws Exception {
        	for (int i = 0; i < 100; i++) {
                value.set(value.get() - 1);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
            return null;
        }
    }
}

package ch.fhnw.richards.topic11_Threads.solutions.CountUpDown_Tasks;

import javafx.application.Application;
import javafx.stage.Stage;

public class CountUpDown extends Application {
	private CountUpDown_View view;
	private CountUpDown_Controller controller;
	private CountUpDown_Model model;

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Note the dependencies between model, view and controller. Additionally,
	 * the view needs the primaryStage created by JavaFX.
	 */
	@Override
	public void start(Stage primaryStage) {
		// Initialize the GUI
		model = new CountUpDown_Model();
		view = new CountUpDown_View(primaryStage, model);
		controller = new CountUpDown_Controller(model, view);

		// Display the GUI after all initialization is complete
		view.start();
	}

	/**
	 * The stop method is the opposite of the start method. It provides an
	 * opportunity to close down the program gracefully, when the program has
	 * been closed.
	 */
	@Override
	public void stop() {
		if (view != null)
			view.stop();
	}
}

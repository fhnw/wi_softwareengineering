package ch.fhnw.richards.topic11_Threads.solutions.CountUpDown_Threads;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

public class CountUpDown_Controller {
    final private CountUpDown_Model model;
    final private CountUpDown_View view;

    protected CountUpDown_Controller(CountUpDown_Model model, CountUpDown_View view) {
        this.model = model;
        this.view = view;

        // Update the view whenever the model value changes.
        model.getValue().addListener((observable, oldValue, newValue) -> {
            updateGUI((Integer) newValue);
        });

        // register ourselves to listen for button clicks
        view.btnUp.setOnAction( e -> {
            model.countUp();
        });

        // register ourselves to listen for button clicks
        view.btnDown.setOnAction( e -> {
            model.countDown();
        });

        // register ourselves to handle window-closing event
        view.getStage().setOnCloseRequest( e -> {
            view.stop();
            Platform.exit();
        });
    }

    /**
     * Ensure the update happens on the JavaFX Application Thread,
     * by using Platform.runLater()
     */
    private void updateGUI(int newValue) {
        Platform.runLater(() -> {
            view.lblNumber.setText(Integer.toString((Integer) newValue));
        });
    }
}

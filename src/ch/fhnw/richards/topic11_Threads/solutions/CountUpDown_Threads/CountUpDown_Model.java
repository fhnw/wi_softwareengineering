package ch.fhnw.richards.topic11_Threads.solutions.CountUpDown_Threads;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.concurrent.Task;

public class CountUpDown_Model {
    private SimpleIntegerProperty value;
    
    protected CountUpDown_Model() {
        value = new SimpleIntegerProperty(0);
    }

    public SimpleIntegerProperty getValue() {
        return value;
    }
    
    public void countUp() {
        Thread task = new CountUpTask();
        task.start();
    }

    public void countDown() {
    	Thread task = new CountDownTask();
        task.start();
    }

    private class CountUpTask extends Thread {
        @Override
        public void run() {
        	for (int i = 0; i < 100; i++) {
                value.set(value.get() + 1);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    private class CountDownTask extends Thread {
        @Override
        public void run() {
        	for (int i = 0; i < 100; i++) {
                value.set(value.get() - 1);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
        }
    }
}

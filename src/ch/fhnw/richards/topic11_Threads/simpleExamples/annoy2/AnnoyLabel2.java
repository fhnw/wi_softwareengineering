package ch.fhnw.richards.topic11_Threads.simpleExamples.annoy2;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

public class AnnoyLabel2 extends Label implements Runnable {
	String name;
	Thread thread;
	volatile boolean stopThread;
	int sleepTime;
	private Color color = Color.WHITE;
	
	AnnoyLabel2(String name, int sleepTime) {
		super();
		this.name = name;
		this.sleepTime = sleepTime;
	}
	
	@Override
	public void run() {
		while (!stopThread) {
			if (color.equals(Color.WHITE)) {
				color = Color.BLACK;
			} else {
				color = Color.WHITE;
			}
			changeStyle(this, color);
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
			}
		}
	}
	
	private void changeStyle(Node element, Color color) {
		Platform.runLater(() -> {
			if (color.equals(Color.WHITE)) {
				element.getStyleClass().remove("white");
				element.getStyleClass().add("black");
			} else {
				element.getStyleClass().remove("black");
				element.getStyleClass().add("white");
			}
		});
	}

	boolean isAlive() {
		if (thread == null) {
			return false;
		} else {
			return thread.isAlive();
		}
	}
	
	void start() {
		thread = new Thread(this, name);
		thread.setDaemon(true);
		stopThread = false;
		thread.start();
	}
	
	void stop() {
		stopThread = true;
		thread.interrupt();
	}
}

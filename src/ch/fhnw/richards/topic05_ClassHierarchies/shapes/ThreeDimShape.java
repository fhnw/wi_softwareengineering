package ch.fhnw.richards.topic05_ClassHierarchies.shapes;

public abstract class ThreeDimShape extends Shape {
	private int depth;
	private int zPos;
	
	public abstract double volume();
	
	public ThreeDimShape(String name) {
		super(name);
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public int getzPos() {
		return zPos;
	}

	public void setzPos(int zPos) {
		this.zPos = zPos;
	}

}

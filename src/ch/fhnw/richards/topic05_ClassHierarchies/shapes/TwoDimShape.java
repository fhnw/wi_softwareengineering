package ch.fhnw.richards.topic05_ClassHierarchies.shapes;

public abstract class TwoDimShape extends Shape {

	public abstract double circumference();
	
	public TwoDimShape(String name) {
		super(name);
	}
}

package ch.fhnw.richards.topic05_ClassHierarchies.shapes;

public class DiscoverClassesOfShapes {

	public static void main(String[] args) {
		// Create a shape
		Shape shape = new Block("block");
	
		// Get the class of shape
		System.out.println("Class of shape is " + shape.getClass().toString());
		
		// Test with "getClass"
		System.out.println("Testing with 'getClass': is this a Shape?         " + shape.getClass().equals(Shape.class));			
		System.out.println("Testing with 'getClass': is this a ThreeDimShape? " + shape.getClass().equals(ThreeDimShape.class));			
		System.out.println("Testing with 'getClass': is this a Block?         " + shape.getClass().equals(Block.class));			
		System.out.println("Testing with 'getClass': is this a Cube?          " + shape.getClass().equals(Cube.class));			
		
		// Test shape with "instanceof"
		System.out.println("\nShape in shape is instance of:");
		System.out.println("Class Shape         " + (shape instanceof Shape));
		System.out.println("Class ThreeDimShape " + (shape instanceof ThreeDimShape));
		System.out.println("Class Block         " + (shape instanceof Block));
		System.out.println("Class Cube          " + (shape instanceof Cube));
	}

}

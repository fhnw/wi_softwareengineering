package ch.fhnw.richards.topic05_ClassHierarchies;

class RoundedRect extends Rectangle {
	private double cornerRadius;

	public RoundedRect(float w, float h, float r) {
		super(w, h);
		cornerRadius = r;
	}

	public double getArea() {
		double rectArea = super.getArea();
		double cornerArea = (4 - Math.PI) * cornerRadius * cornerRadius;
		return rectArea - cornerArea;
	}

	public double getCornerRadius() {
		return cornerRadius;
	}

	public void setCornerRadius(double cornerRadius) {
		this.cornerRadius = cornerRadius;
	}
}

package ch.fhnw.richards.topic05_ClassHierarchies.api_example;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class implements a calculator, but depends on an external service to do
 * the actual calculations.
 */
public class Calculator {
	// Reference to external library - note the declaration type!!
	private static Arithmetic externalService;
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter a simple, mathematical equation without parentheses.");
		System.out.println("For example: 13 * 2 - 3");
		String equation = in.nextLine();
		in.close();

		// Parse the equation into its individual operators and operands
		ArrayList<Integer> numbers = new ArrayList<>();
		ArrayList<Character> operations = new ArrayList<>();
		parseEquation(equation, numbers, operations);

		// Get the external library: Note the declaration type!!
		externalService = new Acme_services();

		while (numbers.size() > 1) {
			executeOneOperation(numbers, operations);
		}

		System.out.println("Answer: " + numbers.get(0));
	}

	private static void parseEquation(String equation, ArrayList<Integer> numbers, ArrayList<Character> operations) {
		String[] parts = equation.split(" ");

		// Even entries are numbers, odd entries are operations
		for (int i = 0; i < parts.length; i++) {
			if (i % 2 == 0) {
				numbers.add(Integer.parseInt(parts[i]));
			} else { // operation - should be a single character
				operations.add(parts[i].charAt(0));
			}
		}
	}

	private static void executeOneOperation(ArrayList<Integer> numbers, ArrayList<Character> operations) {
		int operationIndex = 0; // Default: take the first operation

		// Look for * and / - we will execute the first one found.
		// If no * or / exists, then the default of 0 is correct
		boolean found = false;
		for (int i = 0; !found && i < operations.size(); i++) {
			if (operations.get(i) == '*' || operations.get(i) == '/') {
				operationIndex = i;
				found = true;
			}
		}

		int x = numbers.get(operationIndex);
		int y = numbers.remove(operationIndex + 1);
		char operator = operations.remove(operationIndex);
		int result = executeOperation(x, operator, y);
		numbers.set(operationIndex, result);
	}

	// Using the external library
	private static int executeOperation(int x, char operator, int y) {
		int result = 0;
		switch (operator) {
		case '+':
			result = externalService.add(x, y);
			break;
		case '-':
			result = externalService.subtract(x, y);
			break;
		case '*':
			result = externalService.multiply(x, y);
			break;
		case '/':
			result = externalService.divide(x, y);
			break;
		default:
		}
		return result;
	}
}
